# README #

At WonderBill we like to be confident in the code we deliver. To improve our
confidence when deploying code we have extensive end-to-end testing.

### Task Definition ###

We would like you to automate our web app's registration journey. This 
should be performed using a browser automation tool such as Cypress, 
Puppeteer, Nightwatch, etc., use one you are familiar with - we will 
want to know why you chose it! URL of the web app is: 
[my.wonderbill.com](https://my.wonderbill.com).

### Requirements ###

* Runnable through the command line
* One test that successfully creates a new WonderBill user (up to 
requiring email confirmation)
* One test that fails (on purpose!) and will provide useful data for debug 
(think logs, screenshots, videos, etc.)
* Written in JavaScript/TypeScript

### Submission ###

Please provide a URL to a public repository containing your task submission.
Instructions on how to run the script are useful to include.

### Thank you for your time and effort! ###

*Please note, any account created/used during this task is subject to the 
standard terms and conditions found
[here](https://www.wonderbill.com/terms.html).*
